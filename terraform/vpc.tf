data "aws_region" "current" {}

resource "aws_vpc" "app-vpc" {
  cidr_block = var.cidr_block
}