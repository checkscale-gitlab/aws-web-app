resource "aws_internet_gateway" "int-gw" {
  vpc_id = aws_vpc.app-vpc.id
  tags = {
    Name = "int-gw"
  }
}