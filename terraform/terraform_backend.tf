terraform {
  backend "s3" {
    encrypt = true
    bucket = "terraform-remote-state-storage-s3-odagen"
    key = "terraform/storage/tfstate"
    region = "eu-central-1"
  }
}