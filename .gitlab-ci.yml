stages:
  - validate
  - build
  - dockerize
  - plan AWS
  - deploy AWS
  - destroy AWS

cache:
  paths:
    - .m2/repository
    - terraform/.terraform

variables:
  BUILDTAG: ${CI_PIPELINE_ID}-${CI_COMMIT_SHORT_SHA}

.maven_job_template: &maven_job_template
  image: "maven:3.6.3-jdk-11"
  variables:
    MAVEN_CLI_OPTS: "--batch-mode"
    MAVEN_OPTS: "-Dmaven.repo.local=.m2/repository"

.terraform_job_template: &terraform_job
  image:
    name: "hashicorp/terraform:light"
    entrypoint: ["/usr/bin/env", "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin"]
  before_script:
    - mkdir -p ~/.aws
    - echo $AWS_SECRET | base64 -d > ~/.aws/credentials
    - terraform --version
    - cd ${CI_PROJECT_DIR}/terraform
    - terraform init

validate:
  stage: validate
  <<: *terraform_job
  script:
    - terraform validate

build:
  stage: build
  <<: *maven_job_template
  script:
    - mvn -ff $MAVEN_OPTS $MAVEN_CLI_OPTS clean package -DskipTests
  artifacts:
    expire_in: 1 day
    paths:
      - target/*.jar

dockerize:
  stage: dockerize
  <<: *maven_job_template
  script:
    - mvn compile jib:build -Dbuild_tag=${BUILDTAG}

Plan AWS:
  stage: plan AWS
  <<: *terraform_job
  script:
    - terraform plan -var-file="dev/terraform.tfvars"
  when: manual

Deploy AWS:
  stage: deploy AWS
  <<: *terraform_job
  script:
    - terraform apply -auto-approve -var-file="dev/terraform.tfvars" -var="docker_username=${DOCKER_USERNAME}" -var="docker_password=${DOCKER_PASSWORD}"
  when: manual

Destroy AWS:
  stage: destroy AWS
  <<: *terraform_job
  script:
    - terraform destroy -auto-approve -var-file="dev/terraform.tfvars"
  when: manual